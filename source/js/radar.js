

(function (window, document, $, undef) {

    window.log = function(){
        log.history = log.history || [];   // store logs to an array for reference
        log.history.push(arguments);
        if(window.console){
            console.log( Array.prototype.slice.call(arguments) );
        }
    };

    var CHUN;
    
    
    CHUN = window.CHUN || {};

    
    CHUN.actions = {
        skills : function (_element) {
            var $canvas         = _element,
                canvas          = $canvas.get(0),
                canvasWidth     = $canvas.width(),
                canvasHeight    = $canvas.height(),
                context,
                data            = skillsData,
                skillItems      = data.skills,
                itemsCount      = skillItems.length,
                stepRadian      = (2 * (Math.PI))/itemsCount;

            function getRadian (degree) {
                return (Math.PI/180)*degree;
            }

            //Create the basic/base polygon shape of the graphic
            function drawBase () {
                var midX            = canvasWidth / 2,
                    midY            = canvasHeight / 2,
                    radius          = data.radius,
                    offsetAngle     = Math.PI / 2,//angle to offset the starting angle
                    steps           = 5, //increments
                    stepLength      = radius/steps, //length is pixel of an increment/step
                    stepScore       = data.topScore/steps, //increment of score in a score
                    stepRadius,
                    textOffsetBase  = 10,// px to offset text away from the graph
                    initRatioX      = Math.cos(-stepRadian + offsetAngle), // cosine(angle) = adjacent / hypotenuse
                    initRatioY      = Math.sin(-stepRadian + offsetAngle); // sine(angle) = opposite / hypotenuse

                context.strokeStyle = '#aaa';
                context.lineWidth = 0.4;
                context.font = data.fontSize + 'px Arial';
                context.textAlign = 'center';
                context.textBaseline = 'middle';
                context.fillStyle = '#333';
                context.beginPath();

                
                //loop through each step/increment and draw the polygon for at that level
                for(var k = steps; k >= 1; k--) {
                    stepRadius = k * stepLength;
                    
                    //angle start at x axis to the right of the center point of a circle.
                    //pre-position to the coordinate of the last step.
                    context.moveTo(initRatioX * stepRadius + midX, - ( initRatioY * stepRadius ) + midY);
                    
                    //now the first coordinate will be the one aligning to the x axis, which will then be adjusted to
                    //align to the y axis using "offsetAngle"
                    //for each step, loop through and draw the base polygon shape of the graph based on how many items there are.
                    for(var i = 0; i < itemsCount; i++) {
                        var ratioX      = Math.cos(stepRadian * i + offsetAngle),
                            ratioY      = Math.sin(stepRadian * i + offsetAngle),
                            newX        = ratioX * stepRadius + midX,
                            newY        = - ( ratioY * stepRadius ) + midY,
                            textOffset  = textOffsetBase * (Math.abs(ratioX.toFixed(2)) + 1);
                        
                        context.lineTo(newX, newY);
                        
                        //if not outer most step and is the first item, draw step score number
                        if(k !== steps && i === 0) {
                            var thisStepScore = k * stepScore;
                            
                            context.font = '9px Arial';
                            context.fillStyle = '#aaa';
                            context.fillText(thisStepScore, newX + 4, newY - 3);
                        }

                        //if outer most step...
                        //draw line to center point
                        //get and save score coordinate for each item
                        //draw item text
                        if(k === steps){
                            context.lineTo(midX, midY);
                            context.moveTo(newX, newY);
                            
                            var score       = skillItems[i].score / data.topScore,
                                textX       = ratioX * textOffset + newX,
                                textY       = - ( ratioY * textOffset ) + newY;
                            
                            if(score > 1) {
                                score = 1;
                            }

                            skillItems[i].coordinate = {};
                            skillItems[i].coordinate.x = (ratioX * (score * radius)) + midX;
                            skillItems[i].coordinate.y =  - (ratioY * (score * radius)) + midY;
                            
                            context.fillText(skillItems[i].title, textX, textY);
                        }
                    }
                }
                context.stroke();

                drawGraph();

            }//END layoutGraph

            function drawGraph() {
                context.strokeStyle = '#333';
                context.fillStyle = 'rgba(255, 50, 0, 0.4)';
                context.lineWidth = 1;
                context.beginPath();
                context.moveTo(skillItems[itemsCount-1].coordinate.x, skillItems[itemsCount-1].coordinate.y);

                for (var i = 0; i < itemsCount; i++) {
                    context.lineTo(skillItems[i].coordinate.x, skillItems[i].coordinate.y);
                }

                context.stroke();
                context.fill();
            }//END drawGraph

            (function init () {
                if(canvas.getContext) {
                    context = canvas.getContext('2d');
                    
                    if(data !== undef) {
                        drawBase();
                    } else {
                        $canvas.closest('#skills-block').hide();
                    }
                } else {
                    log('Canvas context unavailable');
                }

            })();//END init
            
        },//END skillGraph

        skillsFallback : function () {
            var skillsWrap  = arguments[0],
                skillItems  = skillsWrap.children('.skill-item');
            
            skillItems.each(function () {
                var score   = this.getAttribute('data-score'),
                    item    = $(this);
                
                item.width((score * 10) + '%' );
            });
        }
    };

    (function (){
        // CHUN.events.start();
        //CHUN.common.centerBlocks();

        // CHUN.ui.modal.init('.work-block');

        $('[data-action]').each(function () {
            var elementAction     = this.getAttribute('data-action');

            if(CHUN.actions[elementAction]) {
                CHUN.actions[elementAction]($(this));
            }
            
        });
        
        console.log('here');
        
    }());//END HomeInit
    
}(this, this.document, this.jQuery));








